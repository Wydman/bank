import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AccountTests {

    @Test
    public void testDeposit() {
        Account account = new Account("Milou", "euros");
        account.deposit(1000);

        Assertions.assertEquals("1000.0 euros", account.getCurrentBalance().toString());
    }

    @Test
    public void testWithdraw() {
        Account account = new Account("Milou", "euros");
        account.deposit(1500);
        account.withdraw(2000);
        account.withdraw(500);

        Assertions.assertEquals("1000.0 euros", account.getCurrentBalance().toString());
    }
}
