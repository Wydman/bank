public class Main {

    public static void main(String[] args) {

        Account account = new Account("Obélix", "Sesterces");

        System.out.println(account.getUserName()+" a " +account.getCurrentBalance().toString()+" sur son compte");
        account.deposit(500.5);
        System.out.println(account.getUserName()+" a " +account.getCurrentBalance().toString()+" sur son compte");
        account.withdraw(125.25);
        System.out.println(account.getUserName()+" a " +account.getCurrentBalance().toString()+" sur son compte");
        account.withdraw(1337.0);
        System.out.println(account.getUserName()+" a " +account.getCurrentBalance().toString()+" sur son compte");
    }
}
