public class Account {
    MonetaryAmount monetaryAccount;
    String userName;

    public Account(String ownerName, String deviseName) {
        monetaryAccount = new MonetaryAmount(0, deviseName);
        userName = ownerName;
    }

    public MonetaryAmount getCurrentBalance(){
    return monetaryAccount;
    }

    public void deposit(double amount) {
        monetaryAccount.addAmount(amount);
    }

    public void withdraw(double amount) {
        if (monetaryAccount.getAmount() >= amount){
            monetaryAccount.substractAmount(amount);
        }
    }

    public String getUserName() {
        return userName;
    }
}
