public class MonetaryAmount {
    private double quantity = 0;
    private String devise = " "; // Trick to Add spacer between MonetaryAmount returned values


    public MonetaryAmount(double amount,String name){
        quantity = amount;
        devise += name; // Trick to Add spacer between MonetaryAmount returned values
    }

    public void addAmount(double amount) {
        quantity += amount;
    }

    public void substractAmount(double amount) {
        quantity -= amount;
    }

    public String toString(){
        return (quantity + devise);
    }

    public double getAmount() {
        return quantity;
    }

    public String getDevise(){
        return  devise;
    }
}

